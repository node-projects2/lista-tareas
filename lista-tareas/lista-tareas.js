const fs = require('fs');


let listadoPorHacer = [];

const guardarDB = () => {
    let data = JSON.stringify(listadoPorHacer);

    fs.writeFile('database/data.json', data, (err) => {
        if (err) {
            throw new Error('No se pudo grabar', err);
        }
    });
}

const cargarDB = () => {
    try {
        listadoPorHacer = require('../database/data.json');

    } catch (error) {
        listadoPorHacer = [];
    }
};


const crear = (descripcion) => {

    cargarDB();

    let tarea = {
        descripcion,
        completado: false
    };

    listadoPorHacer.push(tarea);

    guardarDB();

    return tarea;
};

const getListado = () => {

    cargarDB();
    return listadoPorHacer;
};

const actualizar = (descripcion, completado = true) => {
    cargarDB();
    let index = listadoPorHacer.findIndex(tarea => tarea.descripcion === descripcion);

    if (index >= 0) {
        listadoPorHacer[index].completado = completado;
        guardarDB();
        return true;
    } else {
        return false;
    }
};

const borrar = (descripcion) => {
    cargarDB();
    const tarea = listadoPorHacer.find(tarea => tarea.descripcion === descripcion);

    if (!tarea) { return false; }

    listadoPorHacer = listadoPorHacer.filter(tarea => tarea.descripcion !== descripcion);

    guardarDB();
    return true;
};

module.exports = {
    crear,
    getListado,
    actualizar,
    borrar
}